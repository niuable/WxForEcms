<?php

namespace WxSDK\core\model\tpl;

class DataItem
{
    public $value;
    public $color;
    public $key;
    /**
     * @param string $key
     * @param string $value
     * @param string $color
     */
    function __construct($key, $value, $color = NULL)
    {
        $this->color = $color;
        $this->value = $value;
        $this->key = $key;
    }
}
