<?php

namespace WxSDK\core\model\tpl;

class Miniprogram
{
    public $appid;
    public $pagepath;
    /**
     * @param string $appid
     * @param string $pagePath
     */
    function __construct($appid, $pagePath = NULL)
    {
        $this->appid = $appid;
        $this->pagepath = $pagePath;
    }
}
