<?php

namespace WxSDK\core\model\kefu;

class MenuItem
{
    public $id;
    public $content;
    /**
     * @param string $id
     * @param string $content
     */
    function __construct($id, $content)
    {
        $this->content = $content;
        $this->id = $id;
    }
}
