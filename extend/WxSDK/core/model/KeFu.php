<?php

namespace WxSDK\core\model;

class KeFu extends Model
{
    public $kf_account; //账号
    public $nickname;
    public $password;
    /**
     * @param string $account
     * @param string $nickname
     */
    public function __construct($account, $nickname, $password)
    {
        $this->kf_account = $account;
        $this->nickname = $nickname;
        $this->password = $password;
    }
}
