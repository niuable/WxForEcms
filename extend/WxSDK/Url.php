<?php

namespace WxSDK;

use WxSDK\core\common\IApiUrl;

class Url implements IApiUrl
{
    private $template;
    /**
     * @param string $template
     */
    function __construct($template)
    {
        $this->template = $template;
    }
    /**
     * @param string $accessToken
     */
    public function getUrl($accessToken)
    {
        return str_replace("ACCESS_TOKEN", $accessToken, $this->template);
    }
}
